#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTabWidget>
#include <QVBoxLayout> 
#include <QFileDialog>
#include <fstream>
#include <QPlainTextEdit>
#include <qtextstream.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

	editor = ui->tabWidget;
	editor->clear();
	QVBoxLayout *layout = new QVBoxLayout;

	layout->addWidget(editor);
	ui->centralWidget->setLayout(layout);
	QPlainTextEdit * newTextEdit = new QPlainTextEdit;
	editor->addTab(newTextEdit, "New");
	allTextEdits.append(newTextEdit);
	allFiles.append("");

	connect(ui->actionNew_2, SIGNAL(triggered(bool)), this, SLOT(newFile())); 
	connect(ui->actionOpen, SIGNAL(triggered(bool)), this, SLOT(openFile()));
	connect(ui->actionSave, SIGNAL(triggered(bool)), this, SLOT(saveFile()));
	connect(ui->actionSave_As, SIGNAL(triggered(bool)),	this, SLOT(saveAsFile()));

}

void MainWindow::openFile()
{
	QString fileName =
		QFileDialog::getOpenFileName
		(this, tr("Open File"), "C://", "All files (*.*);; Text File (*.txt)");
	
	QPlainTextEdit * newTextEdit = new  QPlainTextEdit;

	QFile file(fileName); 

	if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
		return;

	QTextStream in(&file);
	QString text = "";
	while (!in.atEnd()) {
		
		QString line = in.readLine();
		text += line;
		newTextEdit->appendPlainText(line);
	}

	file.close();

	allTextEdits.append(newTextEdit);
	allFiles.append(fileName);

	QStringList parts = fileName.split("/");
	QString name = parts.at(parts.size() - 1);

	editor->addTab(newTextEdit, name);
	editor->setCurrentWidget(newTextEdit);
}

void MainWindow::saveFile()
{
	int pos = editor->currentIndex();
	if (allFiles[pos] == "")
	{
		saveAsFile();
	}
	else
	{
		//save
		std::ofstream out(allFiles[pos].toStdString());
		QPlainTextEdit * currentTextEdit = allTextEdits[pos];
		out << currentTextEdit->toPlainText().toStdString();
	}
}

void MainWindow::newFile()
{
	int s = allFiles.size() + 1;
	QString str = s + 48;
	QPlainTextEdit * newTextEdit = new QPlainTextEdit;
	editor->addTab(newTextEdit, "New " + str);

	allTextEdits.append(newTextEdit);
	allFiles.append("");

	editor->setCurrentWidget(newTextEdit);
}

void MainWindow::saveAsFile()
{
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"), "",	tr("Text files (*.txt)"));
	QPlainTextEdit * currentTextEdit = allTextEdits[editor->currentIndex()];

	if (fileName != "")
	{
		std::ofstream out(fileName.toStdString());
		out << currentTextEdit->toPlainText().toStdString();

		QStringList parts = fileName.split("/");
		QString name = parts.at(parts.size() - 1);
		editor->setTabText(editor->currentIndex(), name);

		int pos = editor->currentIndex();
		allFiles[pos] = fileName;
	}
}

MainWindow::~MainWindow()
{
    delete ui;
}
